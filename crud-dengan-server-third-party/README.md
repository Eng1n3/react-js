Install json-server:
- npm install json-server

Install package:
- npm install

Running server:
- json-server -w db.json -p 3004
- npm start