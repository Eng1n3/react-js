import React, {Fragment} from "react";

const ListContact = (props) => {

    return (
        <Fragment>
            <tr>
                <th scope="row">{props.no}</th>
                <td>{props.data.nama}</td>
                <td>{props.data.email}</td>
                <td>{props.data.nomor}</td>
                <td>
                    <button onClick={() => props.getId(props.data.id)} className="btn btn-md btn-success badge rounded-pill">
                        Lihat
                    </button>
                </td>
            </tr>
        </Fragment>
    )
}

export default ListContact;