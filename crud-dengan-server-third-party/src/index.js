import React from 'react';
import ReactDOM from 'react-dom';
import Base from "./container/Base/Base";

ReactDOM.render(
  <React.StrictMode>
    <Base />
  </React.StrictMode>,
  document.getElementById('root')
);
