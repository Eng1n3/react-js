import React, {Component, Fragment} from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Index from "../pages/Index/Index";
import About from "../pages/About/About";
import Contact from "../pages/Contact/Contact";
import DetailPost from "../pages/DetailPost/DetailPost";


class Home extends Component {

    render() {
        return (
            <Router>
                <Fragment>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed mb-3">
                        <div className="container">
                            <Link className="navbar-brand" to="/">Data</Link>
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNav">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <Link to="/" className="nav-link active" aria-current="page">Home</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/about" className="nav-link" aria-current="page">About</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/contact" className="nav-link" aria-current="page">Contact</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <Route path="/" exact component={Index}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contact" component={Contact}/>
                    <Route path="/detail-post/:id" component={DetailPost}/>
                </Fragment>
            </Router>
        )
    }
}

export default Home;