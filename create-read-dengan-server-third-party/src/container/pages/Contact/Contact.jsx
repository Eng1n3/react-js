import React, {Component, Fragment} from "react";
import ListContact from "../../../component/ListContact/ListContact";

class Contact extends Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: []
        }
    }

    getDataAPI = () => {
        fetch("http://localhost:3004/posts")
        .then(response => response.json())
        .then(response => {
            this.setState({
                posts: response
            })
        })
        .catch(err => console.log(err));
    }

    goUrl = id => {
        return this.props.history.push(`/detail-post/${id}`);
    }

    componentDidMount() {
        this.getDataAPI();
    }

    render() {
        return (
            <Fragment>
                <div className="container">
                    <h1>Ini contact page</h1>
                    <div className="row">
                        <div className="col-md-12">
                            <button to="#" className="btn btn-md btn-primary">Tambahkan data</button>
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Nomor</th>
                                            <th scope="col">Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.posts.map((post, i) => {
                                                return (
                                                    <ListContact 
                                                        key={post.id}
                                                        no={i+1}
                                                        data={post}
                                                        getId={this.goUrl}
                                                    />
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Contact;